# Relase Note

## [v2.2.2](https://gitlab.cern.ch/YARR/localdb-tools/-/tree/v2.2.1)

### Release: June 8, 2023

* YARR scan rendering bugfix
* Patching recursive component synchronizer


## [v2.2.1](https://gitlab.cern.ch/YARR/localdb-tools/-/tree/v2.2.1)

### Release: June 7, 2023

* bumping to itkdb 0.4.4, mqdbt 2.0.2, mqat 2.0.1
* refreshing components child-parent relation every time of pulling

## [v2.2.0](https://gitlab.cern.ch/YARR/localdb-tools/-/tree/v2.2.0)

### Release: May 31, 2023

### Key Features
* Electrical test QC_v2 support (simple and complex)
* YARR scan support
* Chip config administration
* Module assembly and registration


### Minor Features
* ITkPD attachment download func with `itkdb >= 0.4.3`
* multithreaded attachment uploading
* Chcking complex analysis for all FEs of a module at once
* Lighter plotting for yarr scans; auto log-scale in plotting; color scale optimized
* QC analysis recycling button
* Installing script maintained
    

## [v2.0.8](https://gitlab.cern.ch/YARR/localdb-tools/-/tree/v2.0.8)

### Release: May 3, 2023

* User message has a dismiss button to remove itself.
* requiring itkdb 0.4.1


## [v2.0.7](https://gitlab.cern.ch/YARR/localdb-tools/-/tree/v2.0.7)

### Release: Apr 19, 2023

* fixing the EOS attachment for site qualification


## [v2.0.6](https://gitlab.cern.ch/YARR/localdb-tools/-/tree/v2.0.6)

### Release: Apr 11, 2023

* fixing a crash by runNumber not compatible with ObjectId


## [v2.0.5](https://gitlab.cern.ch/YARR/localdb-tools/-/tree/v2.0.5)

### Release: Apr 8, 2023

* fixing component downloader reflecting the PDB format change
* improvement of the user messages exposure


## [v2.0.4](https://gitlab.cern.ch/YARR/localdb-tools/-/tree/v2.0.4)

### Release: Apr 7, 2023

* Removal of fs.chunks and fs.files from SQ collection list
* Fixing the issue in uploading to PD
* Escaping the issue in missing subproject fields in module type downloading


## [v2.0.3](https://gitlab.cern.ch/YARR/localdb-tools/-/tree/v2.0.3)

### Release: Apr 4, 2023

* Bugfix in case of missing sub-components in RecursiveSynchronizer
* A better state and message handling for component downloader


## [v2.0.2](https://gitlab.cern.ch/YARR/localdb-tools/-/tree/v2.0.2)

### Release: Mar 28, 2023

* auxiliary `mqat` files as attachment
* `mqat` output downloadable from webpage
* adaptation to `mqat-1.3.0`
* `mqat` output json file for downloading bracketted
* Release of database SQ


## [v2.0.1](https://gitlab.cern.ch/YARR/localdb-tools/-/tree/v2.0.1)

### Release: Mar 17, 2023

* Fixation of various bugs encountered in `v2.0.0`
* This version has been successfully installed in 3 different users independently: Hide, Giordon, Matthias    



## [v2.0.0](https://gitlab.cern.ch/YARR/localdb-tools/-/tree/v2.0.0)

### Release: Mar 10, 2023

Refurbished version for Site Qualification towards pre-production.

### Supported
    
* A handful of electrical QC tests plugging `module-qc-analysis-tools v1.2.0`
  * User can submit the result of `module-qc-tools` to LocalDB.
  * The QC analysis runs inside the LocalDB server.
  * Interfaces for checking-out tests, stage sign-off and submission to ProductionDB are supported.
* Based on `itkdb v0.4.0`
* Coherent stage transition over component hierarchy
* Experimental automatic Site Qualification button collecting the server's information
* More explanation on [a report during ITk Week March 2023](https://indico.cern.ch/event/1223746/#66-local-database-sq)

### Not supported yet (backward compatibility broken)

* Non-electrical QC tests using QCHelper
* YARR scans for ITkPixV1.1 modules
    

## [v1.6.0](https://gitlab.cern.ch/YARR/localdb-tools/-/tree/ldbtoolv1.6.0)

### Release: Sep 17, 2021

##### YARR : [5ca0199d](https://gitlab.cern.ch/YARR/YARR/-/commit/5ca0199de9799695c7d5046a2bdcdff18d8e6847)


### New features
- Push QC test result to ITkPD
- Pull QC test result from ITkPD
- Show all types of scan result(e.g. source scan, tuning)
- Rename component's serial numbers with re-downloading the list of them

### What you can do with this version
- Provide an admin page
- Manage the information of users and QC testers
- Some user functions (comments, tags)
- Register/download module to/from ITkPD
- Supporting a part of the QC stages in the ProdDB (Module to PCB, Wirebonding)
- Show the results of QC test
  - non-electrical test
    - Bare to PCB Assembly (Optical Inspection, Metrology, Mass)
    - Wire-bonding (Optical Inspection, Sensor IV, SLDO VI, Wirebonding Information, Wirebond Pull Test IrefTrim, Pullup register, Orientation)
  - electrical test
    - YARR rerated scan
    - Pixel failure test
- Sign off QC test
- Upload/download QC test results to/from ITkPD
